# ![CM Angular Template](/src/assets/logo.png) Angular Template

# Getting started

Make sure you have the [Angular CLI](https://github.com/angular/angular-cli#installation) installed globally and are runnen [Node.js](https://nodejs.org/en/download/) version > 10 (latest LTS version is 10.16.3). We use [Yarn](https://yarnpkg.com) to manage the dependencies, so we strongly recommend you to use it. You can install it from [here](https://yarnpkg.com/en/docs/install).

<br />

## Get the Code

```
git clone https://git.sharedservices.cmgroep.local/aurora/cm-angular-template.git cm-angular-template

cd cm-angular-template
```

After cloning the project, run `yarn install` to resolve all dependencies (might take a minute). When finished run `ng serve` for a dev server. Navigate to `https://dev.cmtest.nl:1443/nl-nl/app/cm-angular-template`. The app will automatically reload if you change any of the source files.

<br />

## Building the project

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build. Use the `-test` flag for a test build.

<br />

## Workspace configuration files

All projects within a workspace share a CLI configuration context. The top level of the workspace contains workspace-wide configuration files, configuration files for the root-level application, and subfolders for the root-level application source and test files.

| CONFIG FILES        | PURPOSE                                                                                                                                                                                                                                                                                                                                                                                                               |
| ------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `.editorconfig`     | Configuration for code editors. See [EditorConfig](https://editorconfig.org/).                                                                                                                                                                                                                                                                                                                                        |
| `.gitignore`        | Specifies intentionally untracked files that [Git](https://git-scm.com/) should ignore.                                                                                                                                                                                                                                                                                                                               |
| `README.md`         | Introductory documentation for the root app.                                                                                                                                                                                                                                                                                                                                                                          |
| `angular.json`      | CLI configuration defaults for all projects in the workspace, including configuration options for build, serve, and test tools that the CLI uses, such as [TSLint](https://palantir.github.io/tslint/), [Karma](https://karma-runner.github.io/latest/index.html), and [Protractor](http://www.protractortest.org/#/). For details, see [Angular Workspace Configuration](https://angular.io/guide/workspace-config). |
| `package.json`      | Configures [npm package dependencies](https://angular.io/guide/npm-packages) that are available to all projects in the workspace. See [npm documentation](https://docs.npmjs.com/files/package.json) for the specific format and contents of this file.                                                                                                                                                               |
| `package-lock.json` | Provides version information for all packages installed into node_modules by the npm client. See [npm documentation](https://docs.npmjs.com/files/package-lock.json) for details. If you use the yarn client, this file will be [yarn.lock](https://yarnpkg.com/lang/en/docs/yarn-lock/) instead.                                                                                                                     |
| `src/`              | Source files for the root-level application project.                                                                                                                                                                                                                                                                                                                                                                  |
| `node_modules/`     | Provides [npm packages](https://angular.io/guide/npm-packages) to the entire workspace. Workspace-wide node_modules dependencies are visible to all projects.                                                                                                                                                                                                                                                         |
| `tsconfig.json`     | Default [TypeScript](https://www.typescriptlang.org/) configuration for projects in the workspace.                                                                                                                                                                                                                                                                                                                    |
| `tslint.json`       | Default [TSLint](https://palantir.github.io/tslint/) configuration for projects in the workspace.                                                                                                                                                                                                                                                                                                                     |

<br />

## Application project files

By default, the CLI command ng new my-app creates a workspace folder named "my-app" and generates a new application skeleton in a src/ folder at the top level of the workspace. A newly generated application contains source files for a root module, with a root component and template.

When the workspace file structure is in place, you can use the ng generate command on the command line to add functionality and data to the application. This initial root-level application is the default app for CLI commands (unless you change the default after creating [additional apps](https://angular.io/guide/file-structure#multiple-projects)).

<br />

> Besides using the CLI on the command line, you can also use an interactive development environment like [Angular Console](https://angularconsole.com/), or manipulate files directly in the app's source folder and configuration files.

<br />

For a single-application workspace, the src/ subfolder of the workspace contains the source files (application logic, data, and assets) for the root application. For a multi-project workspace, additional projects in the projects/ folder contain a project-name/src/ subfolder with the same structure.

<br />

### Application project files

Files at the top level of src/ support testing and running your application. Subfolders contain the application source and application-specific configuration.

| APP SUPPORT FILES | PURPOSE                                                                                                                                                                                                                                                                                                                                                                                  |
| ----------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `app/`            | Contains the component files in which your application logic and data are defined.                                                                                                                                                                                                                                                                                                       |
| `assets/`         | Contains image and other asset files to be copied as-is when you build your application.                                                                                                                                                                                                                                                                                                 |
| `environments/`   | Contains build configuration options for particular target environments. By default there is an unnamed standard development environment and a production ("prod") environment. You can define additional target environment configurations.                                                                                                                                             |
| `index.html`      | The main HTML page that is served when someone visits your site. The CLI automatically adds all JavaScript and CSS files when building your app, so you typically don't need to add any `<script>` or `<link>` tags here manually.                                                                                                                                                       |
| `main.ts`         | The main entry point for your application. Compiles the application with the [JIT compiler](https://angular.io/guide/glossary#jit) and bootstraps the application's root module (AppModule) to run in the browser. You can also use the [AOT compiler](https://angular.io/guide/aot-compiler) without changing any code by appending the --aot flag to the CLI build and serve commands. |
| `polyfills.ts`    | Provides polyfill scripts for browser support.                                                                                                                                                                                                                                                                                                                                           |
| `styles.scss`     | Lists CSS files that supply styles for a project. The extension reflects the style preprocessor you have configured for the project.                                                                                                                                                                                                                                                     |

<br />

Inside the src/ folder, the app/ folder contains your project's logic and data. Angular components, templates, and styles go here.

<br />

| SRC/APP/ FILES              | PURPOSE                                                                                                                                                                                                                                                             |
| --------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `app/app.component.ts`      | Defines the logic for the app's root component, named AppComponent. The view associated with this root component becomes the root of the view [hierarchy](https://angular.io/guide/glossary#view-hierarchy) as you add components and services to your application. |
| `app/app.component.html`    | Defines the HTML template associated with the root AppComponent.                                                                                                                                                                                                    |
| `app/app.component.css`     | Defines the base CSS stylesheet for the root AppComponent.                                                                                                                                                                                                          |
| `app/app.component.spec.ts` | Defines a unit test for the root AppComponent.                                                                                                                                                                                                                      |
| `app/app.module.ts`         | Defines the root module, named AppModule, that tells Angular how to assemble the application. Initially declares only the AppComponent. As you add more components to the app, they must be declared here.                                                          |

Powered by [![Brought to you by CM.com](/src/assets/logo.png)](https://cm.com)

import * as fs from "fs";

const environmentName = process.argv.slice(2)[0];
const files = ["dist/index.html"];
const forbiddenTerms = ["cmtest", "localhost"];
const environmentFile = `./src/environments/environment.${environmentName}`;

async function doWork() {
    try {
        if (!environmentName) {
            throw Error("No environment specified. Please issue this command with an environment like so: replace.ts <environment>");
        }

        console.log(`Using environment ${environmentName}`);
        const env = (await import(environmentFile)).environment;

        console.log(`---`);

        files.forEach(file => {
            const contents = fs.readFileSync(file).toString().split(/\r|\n/).filter(l => l).map(l => l.trim());

            const newContents = contents.map(line => {
                if (line.match(/cmtest\.nl/g)) {
                    const newLine = line.replace(/cmtest\.nl/g, env.production ? "cm.com" : "cmtest.nl");

                    console.log(`<<< ${line}`);
                    console.log(`>>> ${newLine}`);
                    console.log(`---`);
                    return newLine;
                } else {
                    return line;
                }
            });

            fs.writeFileSync(file, newContents.join("\r\n"));

            if (environmentName === "prod") {
                forbiddenTerms.forEach(term => {
                    if (contents.indexOf(term) >= 0) {
                        throw Error(`The forbidden term 'cmtest' remained in the file ${file}`);
                    }
                });
            }

        });
        console.log("Replacements complete");
    } catch (err) {
        console.error(err);
        process.exit(1);
    }
}

doWork();

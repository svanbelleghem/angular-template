import * as fs from "fs";

const settingsPath = "./src/app/locale.settings.ts";
const localesPath = "@angular/common/locales";

export interface ILocaleLanguage {
    cmLanguage: string;
    i18n: string;
}

export const writeLocaleSettingsToFile = (languages: ILocaleLanguage[]) => {
    const settings = languages.map((language: ILocaleLanguage) => {
        return {
            locales: `["${language.cmLanguage}"]`,
            settings: `require("${localesPath}/${language.cmLanguage}").default`
        };
    });

    let localeSettingsFile = `export const supportedLocales = [\n`;
    settings.forEach((setting) => {
        localeSettingsFile += `\t{locales: ${setting.locales}, settings: ${setting.settings}},\n`;
    });
    localeSettingsFile += `];\n`;
    localeSettingsFile += `export default supportedLocales;\n`;

    console.log(localeSettingsFile);
    fs.writeFile(settingsPath, localeSettingsFile, (err => {
        if (err) {
            console.log(err);
        }
        console.log(`Wrote file to ${settingsPath}`);
    }));
};

writeLocaleSettingsToFile([{ cmLanguage: "en", i18n: "en_EN" }, { cmLanguage: "nl", i18n: "nl_NL" }]);

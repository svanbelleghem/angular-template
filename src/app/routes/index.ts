import { RouterModule, Routes } from "@angular/router";
import { BaseComponent } from "./base/base.component";
import { HelloWorldComponent } from "./hello-world/hello-world.component";
import { NotFoundComponent } from "./not-found/not-found.component";
export { AppComponent } from "./app/app.component";

const appRoutes: Routes = [
    {
        path: ":accountGuid",
        component: BaseComponent,
        data: { title: "title" },
        children: [
            {
                path: "",
                redirectTo: "overview",
                pathMatch: "full"
            }, {
                path: "overview",
                component: HelloWorldComponent,
            },
        ]
    },
    {
        path: "**",
        component: NotFoundComponent
    }
];

export const RouteComponents = [
    BaseComponent,
    NotFoundComponent,
    HelloWorldComponent
];

export const RouteDefinitions = RouterModule.forRoot(appRoutes, {
    enableTracing: false,
    paramsInheritanceStrategy: "always"
});


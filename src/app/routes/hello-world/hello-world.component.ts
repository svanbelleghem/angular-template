import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { Observable, of, BehaviorSubject } from "rxjs";
import { CdkVirtualScrollViewport } from "@angular/cdk/scrolling";
import { ActivatedRoute, Router } from "@angular/router";
import { HelloWorldService, IApiWorld } from "src/app/clients";

@Component({
    selector: "app-hello-world",
    templateUrl: "./hello-world.component.html",
    styleUrls: ["./hello-world.component.css"]
})
export class HelloWorldComponent implements OnInit, OnDestroy {
    constructor(
        protected route: ActivatedRoute,
        protected router: Router,
        private readonly helloWorldService: HelloWorldService
    ) {

    }

    ngOnInit() {
    }

    ngOnDestroy(): void {
    }
}

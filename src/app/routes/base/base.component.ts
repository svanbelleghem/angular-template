import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subject } from "rxjs";
import { TranslationService, AccountService } from "src/app/services";

@Component({
    selector: "app-base",
    templateUrl: "./base.component.html",
    styleUrls: ["./base.component.scss"]
})
export class BaseComponent implements OnInit, OnDestroy {
    private onDestroy$ = new Subject();

    public showJoyrideDialog = false;
    public hasManuallyClosedJoyrideDialog = false;
    public readonly urlParts = window.location.href;

    sortOptions = {
        DateDesc: "date desc",
        DateAsc: "date",
        NameDesc: "name desc",
        NameAsc: "name",
    };

    storageKeys = {
        sort: "cm_angular_template_sort",
        show: "cm_angular_template_show",
        view: "cm_angular_template_view"
    };

    constructor(
        public readonly translationService: TranslationService,
        public readonly accountService: AccountService
    ) {
    }

    ngOnInit() {
    }

    public closeJoyrideDialog() {
        localStorage.setItem("cm-channels-first-time", "false");
        this.hasManuallyClosedJoyrideDialog = true;
    }

    ngOnDestroy() {
        this.onDestroy$.next();
    }
}

import { Component } from "@angular/core";
import { BaseDialogComponent } from "../base-dialog/base-dialog.component";
import { DialogService } from "src/app/services";

@Component({
    selector: "app-confirm-dialog",
    templateUrl: "confirmDialog.content.html"
})
export class ConfirmDialogComponent extends BaseDialogComponent {
    public headerText: string;
    public bodyText: string;
    public confirmText: string;
    public confirmCheckbox: boolean;
    public confirmCheckboxValue = false;

    constructor(dialogService: DialogService) {
        super(dialogService);
    }

    // tslint:disable-next-line:no-any
    setModal(id: string, data: any) {
        this.id = id;
        this.headerText = data.headerText;
        this.bodyText = data.bodyText;
        this.confirmText = data.confirmText;
        this.confirmCheckbox = data.confirmCheckbox;
    }
}

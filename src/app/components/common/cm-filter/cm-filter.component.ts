import { Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";

@Component({
    selector: "app-cm-filter",
    templateUrl: "./cm-filter.component.html",
    styleUrls: ["./cm-filter.component.css"]
})
export class CmFilterComponent implements OnInit, OnChanges, OnDestroy {
    @Input() isActive: boolean;

    @Output() setFilterEvent = new EventEmitter<boolean>();
    @Output() closeEvent = new EventEmitter<boolean>();

    @ViewChild("cmFilter", {static: true}) cmFilter: ElementRef;

    private onDestroy$ = new Subject();

    sortOptions = {
        DateDesc: "date desc",
        DateAsc: "date",
        NameDesc: "name desc",
        NameAsc: "name",
    };

    showOptions = {
        ShowAll: "show-all",
        Favorite: "favorite",
    };

    storageKeys = {
        sort: "cm_angular_template_sort",
        show: "cm_angular_template_show",
        view: "cm_angular_template_view"
    };

    filterOptions: IFilterOptions;

    constructor(
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.filterOptions = <IFilterOptions>{
            show: this.route.snapshot.queryParamMap.get("show") || "show-all",
            sort: this.route.snapshot.queryParamMap.get("sort") || this.sortOptions.DateDesc,
            // view: ""
        };

        const ctx = this;
        window.addEventListener(
            "cm-filter-closed",
            function () {
                ctx.close();
            }
        );
    }

    ngOnDestroy() {

    }

    private close() {
        this.closeEvent.emit(true);
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.isActive.currentValue === true) {
            this.cmFilter.nativeElement.open();
        }

        if (changes.isActive.previousValue === true && changes.isActive.currentValue === false) {
            this.cmFilter.nativeElement.close();
        }
    }

    public setFilter(showOption?: string, sortOption?: string) {
        const data = {
            show: showOption || this.filterOptions.show,
            sort: sortOption || this.filterOptions.sort,
            view: this.filterOptions.view,
        };

        this.filterOptions = data;

        // const preferences: Array<{ Key: string; Value: string; }> = Object.entries(data).map(([k, v]) => {
        //     return { Key: this.storageKeys[k], Value: v };
        // });
        // const [show, sort, view] = preferences;

        // TODO Store settings
        // this.identityService.getIdentity().pipe(
        //     takeUntil(this.onDestroy$)
        //     // tslint:disable-next-line: no-shadowed-variable
        // ).subscribe((identity: IIdentity) => {
        //     combineLatest(
        //         this.personPreferenceService.setOption({ personGuid: identity.UserData.PersonGuid, preference: show }),
        //         this.personPreferenceService.setOption({ personGuid: identity.UserData.PersonGuid, preference: sort }),
        //         // this.personPreferenceService.setOption({ personGuid: identity.UserData.PersonGuid, preference: view })
        //     ).pipe(
        //         untilDestroyed(this)
        //     ).subscribe((_) => {
        // const options = <IFilterOptions>{
        //     show: data.show,
        //     sort: data.sort,
        //     // view: data.view,
        // };

        this.setFilterEvent.emit(true);

        // this.router.navigate([], {
        //     relativeTo: this.route,
        //     queryParams: options,
        //     replaceUrl: true,
        //     queryParamsHandling: "merge"
        // });
        //     });
        // });
    }
}

export interface IFilterOptions {
    show: string;
    sort: string;
    view: string;
    changed?: boolean;
}

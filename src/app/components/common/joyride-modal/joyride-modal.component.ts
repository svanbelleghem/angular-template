import {
    Component,
    Output,
    EventEmitter,
    HostListener,
    OnInit,
    OnDestroy
} from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";
import { tap, takeUntil } from "rxjs/operators";

interface JoyridePage {
    title: string;
    body: string;
    videoUrl: string;
}

interface ActivePage {
    cursor: number;
    page: JoyridePage;
    hasPrevious: boolean;
    hasNext: boolean;
    amount: number;
}

const enum keyCodes {
    Escape = 27,
    LeftArrow = 37,
    RightArrow = 39
}

const pages: JoyridePage[] = [
    {
        title: "onboarding.title.1",
        body: "onboarding.body.1",
        videoUrl: "assets/videos/step1.mp4",
    },
    {
        title: "onboarding.title.2",
        body: "onboarding.body.2",
        videoUrl: "assets/videos/step2.mp4",
    },
    {
        title: "onboarding.title.3",
        body: "onboarding.body.3",
        videoUrl: "assets/videos/step3.mp4",
    }
];

@Component({
    selector: "app-joyride-modal",
    templateUrl: "./joyride-modal.component.html",
    styleUrls: ["./joyride-modal.component.css"]
})
export class JoyrideModalComponent implements OnInit, OnDestroy {
    @Output()
    public close = new EventEmitter<void>();
    public pages = pages;
    private activePage?: ActivePage;
    public activePage$ = new BehaviorSubject<ActivePage>({
        cursor: 0,
        hasPrevious: false,
        hasNext: !!this.pages[1],
        page: this.pages[0],
        amount: pages.length
    });

    private onDestroy$ = new Subject<void>();
    private activePageObserver = this.activePage$.pipe(
        tap(activePage => {
            this.activePage = activePage;
            return activePage;
        })
    );

    ngOnDestroy(): void {
        this.onDestroy$.next();
    }

    ngOnInit(): void {
        this.activePageObserver.pipe(takeUntil(this.onDestroy$)).subscribe();
    }

    @HostListener("window:keydown", ["$event"])
    onkeydown(e: KeyboardEvent) {
        if (e) {
            switch (true) {
                case e.keyCode === keyCodes.Escape:
                    this.handleClose();
                    break;
                case this.activePage && e.keyCode === keyCodes.LeftArrow:
                    this.goToPrevious(this.activePage);
                    break;
                case this.activePage && e.keyCode === keyCodes.RightArrow:
                    this.goToNext(this.activePage);
                    break;
            }
        }
    }

    goToNext(activePage: ActivePage) {
        this.goToPage(activePage.cursor + 1);
    }

    goToPrevious(activePage: ActivePage) {
        this.goToPage(activePage.cursor - 1);
    }

    goToPage(cursor: number) {
        const page = this.pages[cursor];
        if (page) {
            this.activePage$.next({
                cursor,
                page,
                hasNext: !!this.pages[cursor + 1],
                hasPrevious: !!this.pages[cursor - 1],
                amount: this.pages.length
            });
        }
    }

    public handleClose() {
        this.close.emit();
    }
}

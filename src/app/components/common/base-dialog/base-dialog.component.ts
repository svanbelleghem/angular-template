import { Component } from "@angular/core";
import { DialogService } from "src/app/services";

@Component({
    selector: "app-base-dialog",
    templateUrl: "base-dialog.content.html"
})
export class BaseDialogComponent {
    protected id: string;

    constructor(protected dialogService: DialogService) { }

    setModal(id: string, data: {}) {
        this.id = id;
    }

    public get Id() {
        return this.id;
    }

    public cancel() {
        this.dialogService.Cancelled = true;
        this.dialogService.removeComponent();
    }

    public confirm(data?: {}) {
        this.dialogService.Cancelled = false;
        this.dialogService.removeComponent();
        this.dialogService.emitData(false, data);
    }
}

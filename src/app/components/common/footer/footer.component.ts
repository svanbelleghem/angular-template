import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
    selector: "app-footer",
    templateUrl: "./footer.component.html",
    styleUrls: ["./footer.component.css"]
})
export class FooterComponent implements OnInit {

    @Input() selected: any[];

    @Output() selectAllEvent = new EventEmitter();
    @Output() deselectAllEvent = new EventEmitter();
    @Output() deleteSelectedEvent = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    selectAll() {
        this.selectAllEvent.emit();
    }

    deselectAll() {
        this.deselectAllEvent.emit();
    }

    deleteSelected() {
        this.deleteSelectedEvent.emit();
    }
}

import { Component, EventEmitter, OnInit, Output, Input } from "@angular/core";

@Component({
    selector: "app-action-bar-options",
    templateUrl: "./action-bar-options.component.html",
    styleUrls: ["./action-bar-options.component.scss"]
})
export class ActionBarOptionsComponent implements OnInit {

    @Input() hideFilterOption: boolean;

    showFilterOptions: boolean;

    @Output() setFilterEvent = new EventEmitter<boolean>();
    @Output() clickEvent = new EventEmitter<boolean>();

    constructor() {
    }

    ngOnInit() {
    }

    public applyFilterOptions() {
        this.setFilterEvent.emit(true);
    }

    public clicked() {
        this.clickEvent.emit(true);
    }
}

export interface IFilterOptions {
    show: string;
    sort: string;
    view: string;
    changed?: boolean;
}

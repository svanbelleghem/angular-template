import { ApplicationRef, ComponentFactoryResolver, ComponentRef, EmbeddedViewRef, Injectable, Injector } from "@angular/core";
import { Subject, Subscription } from "rxjs";

@Injectable()
// tslint:disable:no-any
export class DialogService {
    private componentRef: ComponentRef<any>;
    private id: string;
    private cancelled: boolean;
    private emitDataSubject: Subject<DialogResult>;

    constructor(
        private componentFactoryResolver: ComponentFactoryResolver,
        private appRef: ApplicationRef,
        private injector: Injector
    ) { }

    /**
     * Creates a new DialogComponent.
     * @param component Class name of a DialogComponent that inherits BaseDialogComponent.
     * @param data The data to be set.
     * @param id Unique id of a dialog component.
     */
    public async createDialogComponent(
        component: any,
        data: any,
        id: string
    ): Promise<DialogResult> {
        this.emitDataSubject = new Subject<DialogResult>();
        this.id = id;
        let dialog = document.getElementById(id);

        // 1. Create a component reference from the component
        this.componentRef = this.componentFactoryResolver
            .resolveComponentFactory(component)
            .create(this.injector);

        // 2. Set data into DialogComponent.
        this.componentRef.instance.setModal(id, data);

        // 3. Attach component to the appRef so that it's inside the ng component tree
        this.appRef.attachView(this.componentRef.hostView);

        if (!dialog) {
            // 4. Get DOM element from component
            const domElem = (this.componentRef.hostView as EmbeddedViewRef<any>)
                .rootNodes[0] as HTMLElement;

            // 4. Append DOM element to the body
            document.body.appendChild(domElem);
            await new Promise(resolve => setTimeout(resolve, 10));

            dialog = document.getElementById(id);
            const ctx = this;
            dialog.addEventListener(
                "cm-dialog-closed",
                function () {
                    ctx.removeComponent();
                },
                false
            );
        }

        this.cancelled = true;

        // TODO fix weird cast
        (dialog as any).open();
        (dialog as any).removeAttribute("isClosed", true);

        // For reasons unknown a Subject is not directly convertible to a promise
        // ref: https://github.com/Reactive-Extensions/RxJS/issues/1088
        return new Promise<DialogResult>((resolve, reject) => {
            let subscription: Subscription;
            subscription = this.emitDataSubject.subscribe(result => {
                subscription.unsubscribe();
                resolve(result);
            });
        });
    }

    public removeComponent() {
        const dialog = document.getElementById(this.id) as any;
        if (!dialog.getAttribute("isClosed")) {
            dialog.setAttribute("isClosed", true);
            dialog.close();
        } else {
            if (this.cancelled) {
                this.emitData(true, null);
            }
            this.appRef.detachView(this.componentRef.hostView);
            this.componentRef.destroy();
            document.body.removeChild(dialog);
        }
    }

    public set Cancelled(cancelled: boolean) {
        this.cancelled = cancelled;
    }

    public emitData(cancelled: boolean, data: unknown) {
        const result = {
            cancelled: cancelled,
            data: data
        };

        this.emitDataSubject.next(result);
    }
}

export interface DialogResult {
    cancelled: boolean;
    data: unknown;
}

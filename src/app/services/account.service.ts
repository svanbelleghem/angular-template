import { Injectable } from "@angular/core";

const EmptyGuid = "000000-0000-0000-0000-000000000000";

function getLogicalAccountId(): string {
    const appParts = window.location.pathname.split("/");
    const guid = appParts[1] === "app" ? appParts[3] : appParts[4];
    if (!guid) {
        console.error(`Expected account id, but found nothing on the url.`);
        return EmptyGuid;
    }
    if (!/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/.test(guid.toLowerCase())) {
        console.error(`Expected account id, but found /${guid}/ on the url.`);
        return EmptyGuid;
    }
    return guid.toLowerCase();
}

function sendHttp<T>(method: ("GET" | "POST"), url: string, body?: {}): Promise<T> {
    return new Promise(function (resolve, reject) {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url);
        xhr.withCredentials = true;
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(JSON.parse(xhr.response));
            } else {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            }
        };
        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            });
        };
        xhr.setRequestHeader("content-type", "application/json");
        xhr.send(JSON.stringify(body));
    });
}

@Injectable({ providedIn: "root" })
export class AccountService {
    private static readonly _logicalAccountId: string = getLogicalAccountId();

    private static _logicalAccount: ILogicalAccount;

    public get logicalAccount(): ILogicalAccount {
        return AccountService._logicalAccount;
    }

    public static async preload(baseUrl: string, redirectIfInvalid = true): Promise<boolean> {
        if (!baseUrl.endsWith("/")) {
            baseUrl += "/";
        }

        try {
            if (!this._logicalAccountId || this._logicalAccountId === EmptyGuid) {
                throw new Error("Account not specified.");
            }

            this._logicalAccount = await sendHttp<ILogicalAccount>(
                "GET", `${baseUrl}accounts/v2.0/accounts/${this._logicalAccountId}?includeLinks=true`
            );
            return true;
        } catch {
            if (redirectIfInvalid) {
                const whereTo = await sendHttp<{ goto: string }>("POST", `${baseUrl}selectedaccount/v1.0/whereto`, {
                    "ReturnUrl": `${window.location.origin}${document.querySelector("base").getAttribute("href") || "/"}[accountGuid]`,
                    "AccountType": "logicalaccount"
                });
                window.location.href = whereTo.goto;
            }
            return false;
        }
    }

    public technicalLink(technicalLinkTypeId: string): ITechnicalLink {
        technicalLinkTypeId = String(technicalLinkTypeId);
        return this.logicalAccount.TechnicalLinks.find(
            l => l.TechnicalLinkTypeID === technicalLinkTypeId
        );
    }

    constructor() {
    }
}

export interface ILogicalAccount {
    ID?: string;
    OrganizationID?: number;
    Name?: string;
    BillingTypeID?: string;
    WalletID?: string;
    CreatedOn?: Date;
    DeletedOn?: Date;
    TechnicalLinks?: ITechnicalLink[];
    Capabilities?: string[];
}

export interface ITechnicalLink {
    ID?: string;
    AccountID?: string;
    TechnicalLinkTypeID?: string;
    Keys?: { [key: string]: string };
    DeletedOn?: Date;
    CreatedOn?: Date;
}

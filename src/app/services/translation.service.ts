import { Injectable } from "@angular/core";

const DefaultLanguage = "en-bg";

function getLanguage(): string {
    const languageId = (window.location.pathname.split("/", 2)[1] || "").toLowerCase();
    return (languageId === "app" ? undefined : languageId) || DefaultLanguage;
}

function getHttp<T>(url: string): Promise<T> {
    return new Promise(function (resolve, reject) {
        const xhr = new XMLHttpRequest();
        xhr.open("GET", url);
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(JSON.parse(xhr.response));
            } else {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            }
        };
        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            });
        };
        xhr.send();
    });
}

@Injectable({ providedIn: "root" })
export class TranslationService {
    private static appData: IApiAppData;
    private static languageRules: Function[];
    private static productionMode: boolean;

    private static readonly _language: string = getLanguage();

    public get language(): string {
        return TranslationService._language;
    }

    public static get Language(): string {
        return this._language;
    }

    public static get DefaultLanguage(): string {
        return DefaultLanguage;
    }

    public static EnableProdMode() {
        this.productionMode = true;
    }

    public static async preload(baseUrl: string, scopeId: string): Promise<string> {
        if (!baseUrl.endsWith("/")) {
            baseUrl += "/";
        }

        const appData = await getHttp<IApiAppData>(`${baseUrl}translations/v1.0/scopes/${scopeId}/app-data/${this.Language}`);

        this.languageRules = appData.rules.map(ruleSet => new Function("n",
            `"use strict"; return ` + ruleSet.map((rule, ix) => `(${rule}) ? ${ix + 1} : `).join() + "0;"
        ));

        this.appData = appData;

        return this._language;
    }

    constructor() {
    }

    public translate(tag: string = "", number: number = 0): string {
        if (!tag) {
            return "";
        }

        tag = String(tag);
        number = Number(number);

        const keyData = TranslationService.appData.keys[tag];
        if (!keyData) {
            console.warn(`Unknown tag '${tag}'`);
            return TranslationService.productionMode ? tag : `«‹${tag}›»`;
        }
        if (!keyData.forms || keyData.forms.length === 0) {
            console.warn(`No translation for tag '${tag}'`);
            return TranslationService.productionMode ? tag : `«${tag}»`;
        }

        // if there is only one form, there's no point in evaluating the number
        if (keyData.forms.length === 1) {
            return keyData.forms[0];
        }

        const rule = TranslationService.languageRules[keyData.language || 0];
        const form = !rule ? 0 : rule(number);

        // if the demanded form is not available, we fall back to default plural
        const result = keyData.forms[form];
        return result === undefined ? keyData.forms[0] : result;
    }
}

interface IApiAppData {
    languages?: string[];
    rules?: string[][];
    keys?: { [key: string]: IApiAppKeyData };
}

interface IApiAppKeyData {
    forms?: string[];
    language?: number;
}

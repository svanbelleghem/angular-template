
import { Injectable } from "@angular/core";

interface DataTarget {
    [key: string]: (data: unknown) => void;
}

function loadJsonP<T>(url: string, target: string, parameter = "callback", ): Promise<T> {
    const script = document.createElement("script");
    const timeout = setTimeout(script.onerror, 5000);
    function detach() {
        clearTimeout(timeout);
        (window as unknown as DataTarget)[target] = undefined;
        document.head.removeChild(script);
    }
    const p = new Promise<T>((resolve, reject) => {
        (window as unknown as DataTarget)[target] = (data: T) => {
            detach();
            resolve(data);
        };
        script.onerror = (reason) => {
            detach();
            reject(reason);
        };
    });
    document.head.appendChild(script);
    script.src = `${url}${/\?/.test(url) ? "&" : "?"}${parameter}=window.${target}`;
    return p;
}

@Injectable({ providedIn: "root" })
export class IdentityService {
    private static _identity: IIdentity;

    public get identity(): IIdentity {
        return IdentityService._identity;
    }

    public static async preload(authUrl: string): Promise<void> {
        if (!authUrl.endsWith("/")) {
            authUrl += "/";
        }

        this._identity = await loadJsonP<IIdentity>(`${authUrl}v1.0/identity`, "identityCallback");
    }

    constructor() {
    }
}

export interface IIdentity {
    Name?: string;
    FirstName?: string;
    LastName?: string;
    SignOutUrl?: string;
    UserData: {
        PersonGuid: string;
        Name: string;
        IpAddress?: string;
        AccountGuid?: string;
        AvatarGuid?: string;
        Expiry?: string;
    };
    Claims: {
        name?: string;
        emailaddress?: string;
        authenticationmethod?: string;
        mobilephone?: string;
        windowsaccountname?: string;
    };
    PersonPreferences: {
        PreferredLanguage?: string;
        TimeZone?: string;
        IntDialingPrefix?: string;
    };
    Expiration?: string;
}

import { Pipe, PipeTransform } from "@angular/core";
import { TranslationService } from "src/app/services";

@Pipe({
    name: "trans"
})
export class TranslationPipe implements PipeTransform {
    constructor(private translationService: TranslationService) {
    }

    transform(key: string, number: number = 0): string {
        return this.translationService.translate(key, number);
    }
}

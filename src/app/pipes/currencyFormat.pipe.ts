import { Pipe, PipeTransform } from "@angular/core";
import { TranslationService } from "../services";

@Pipe({ name: "currencyFormat" })
export class CustomCurrencyPipe implements PipeTransform {

    constructor(
        public translationService: TranslationService
    ) { }

    // Usage: {currencyToTransform} | currencyFormat : {currency}
    // {currency} e.g. USD or EUR for $ or €
    // {locale} e.g. en-US or nl-NL based on chosen language

    transform(value: number, currency: string): string {
        if(currency != undefined) {
            const formatter = new Intl.NumberFormat(
                this.translationService.language, {
                    style: "currency",
                    currency: currency,
                }
            );

            return formatter.format(value);
         }
         
         return Intl.NumberFormat().format(value);
    }
}

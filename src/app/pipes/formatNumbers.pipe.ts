import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "formatNumbers" })
export class FormatNumbersPipe implements PipeTransform {

    constructor() {
    }

    transform(num: number) {
        const number = num > 999 ? (num / 1000).toFixed(0) + "k" : num;
        return number;
    }
}

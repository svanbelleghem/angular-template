import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "tag"
})
export class TagPipe implements PipeTransform {

    constructor() {
    }

    transform(source: string, tag: string, value: any) {
        if (!source || !tag) {
            return source;
        }

        source = String(source);
        tag = String(tag);
        value = value === undefined ? "" : String(value);

        return source.replace(`{${tag}}`, value);
    }
}

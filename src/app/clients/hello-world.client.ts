/* tslint:disable */
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable()

export class HelloWorldService {

    public static Url: string;

    constructor(private http: HttpClient) {
        if (!HelloWorldService.Url) {
            throw new Error("HelloWorldService.Url is not set.");
        }

        if (HelloWorldService.Url[HelloWorldService.Url.length - 1] !== "/") {
            HelloWorldService.Url += "/";
        }
    }

    private static query(arg: string[]): string {
        if (!arg) {
            return "";
        }
        let result = "";
        arg.forEach(a => {
            if (a) {
                result += (result ? "&" : "?") + a;
            }
        });
        return result;
    }

    public getWorlds(data: { skip?: number, take?: number, search?: string; }): Observable<IApiWorld[]> {
        let queryParameters: string[] = [];
        if (data && data.search !== undefined && data.search !== null)
            queryParameters.push(`search=${encodeURIComponent(data.search)}`);
        if (data && data.skip !== undefined && data.skip !== null)
            queryParameters.push(`skip=${data.skip}`);
        if (data && data.take !== undefined && data.take !== null)
            queryParameters.push(`take=${data.take}`);

        return this.http
            .get(`${HelloWorldService.Url}helloworld/v1.0/worlds${HelloWorldService.query(queryParameters)}`,
                { withCredentials: true, observe: "response" })
            .pipe(map((r: HttpResponse<IApiWorld[]>) => {
                const result = <any>r.body;
                return result;
            }));
    }

    public addWorld(world: IApiWorld): Observable<IApiWorld> {
        return this.http
            .post(`${HelloWorldService.Url}helloworld/v1.0/worlds`, world,
                { withCredentials: true, observe: "response" })
            .pipe(map((r: HttpResponse<IApiWorld[]>) => {
                const result = <any>r.body;
                return result;
            }));
    }

    public updateWorld(world: IApiWorld): Observable<IApiWorld> {
        return this.http
            .put(`${HelloWorldService.Url}helloworld/v1.0/worlds/${world.Name}`, world,
                { withCredentials: true, observe: "response" })
            .pipe(map((r: HttpResponse<IApiWorld[]>) => {
                const result = <any>r.body;
                return result;
            }));
    }

    public deleteWorld(world: IApiWorld): Observable<string> {
        return this.http
            .delete(`${HelloWorldService.Url}helloworld/v1.0/worlds/${world.Name}`,
                { withCredentials: true, observe: "response" })
            .pipe(map((r: HttpResponse<IApiWorld[]>) => {
                const result = <any>r.body;
                return result;
            }));
    }

    public resetDataStore(): Observable<string> {
        return this.http
            .get(`${HelloWorldService.Url}helloworld/v1.0/worlds/reset`,
                { withCredentials: true, observe: "response" })
            .pipe(map((r: HttpResponse<IApiWorld[]>) => {
                const result = <any>r.body;
                return result;
            }));
    }
}

export class IApiWorld {
    constructor(public Climate: string,
        public ID: string,
        public Diameter: string,
        public HasWater: string,
        public Name: string,
        public Population: string) {
    }
}

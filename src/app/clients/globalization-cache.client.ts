import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map, shareReplay } from "rxjs/operators";
import { IApiCurrency, IApiCountry, GlobalizationService } from ".";

const cacheBufferSize = 1;

@Injectable({
    providedIn: "root"
})

export class GlobalizationCacheService {

    private readonly currenciesCache$: Observable<Array<IApiCurrency>> = this.getApiCurrencies().pipe(
        shareReplay(cacheBufferSize)
    );

    private readonly countriesCache$: Observable<Array<IApiCountry>> = this.getApiCountries().pipe(
        shareReplay(cacheBufferSize)
    );

    constructor(private globalizationService: GlobalizationService) {
    }

    public get currencies(): Observable<Array<IApiCurrency>> {
        return this.currenciesCache$;
    }

    public get countries(): Observable<Array<IApiCountry>> {
        return this.countriesCache$;
    }

    private getApiCurrencies(): Observable<Array<IApiCurrency>> {
        return this.globalizationService.getCurrencies().pipe(
            map(apiCurrencies => apiCurrencies)
        );
    }

    private getApiCountries(): Observable<Array<IApiCountry>> {
        return this.globalizationService.getCountries().pipe(
            map(apiCountries => apiCountries)
        );
    }
}

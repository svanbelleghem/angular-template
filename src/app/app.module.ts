import { APP_BASE_HREF } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { CRMService, GlobalizationService, HelloWorldService } from "./clients";
import { ActionBarOptionsComponent } from "./components/common/action-bar-options/action-bar-options.component";
import { BaseDialogComponent } from "./components/common/base-dialog/base-dialog.component";
import { CmFilterComponent } from "./components/common/cm-filter/cm-filter.component";
import { ConfirmDialogComponent } from "./components/common/confirmDialog/confirmDialog.component";
import { FooterComponent } from "./components/common/footer/footer.component";
import { JoyrideModalComponent } from "./components/common/joyride-modal/joyride-modal.component";
import { SafePipe, TagPipe, TranslationPipe, FormatNumbersPipe, CustomCurrencyPipe } from "./pipes";
import { AppComponent, RouteComponents, RouteDefinitions } from "./routes";
import { DialogService, TranslationService } from "./services";

function getBaseHref(): string {
    const parts = window.location.pathname.split("/", 5);
    const baseParts = parts.slice(0, parts[1] === "app" ? 3 : 4);
    return baseParts.join("/") + "/";
}

@NgModule({
    declarations: [
        AppComponent,
        ...RouteComponents,
        TagPipe,
        SafePipe,
        FormatNumbersPipe,
        CustomCurrencyPipe,
        TranslationPipe,
        ActionBarOptionsComponent,
        BaseDialogComponent,
        CmFilterComponent,
        ConfirmDialogComponent,
        FooterComponent,
        JoyrideModalComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        RouteDefinitions,
    ], exports: [
        TagPipe
    ],
    providers: [
        GlobalizationService,
        CRMService,
        DialogService,
        HelloWorldService,
        SafePipe,
        FormatNumbersPipe,
        CustomCurrencyPipe,
        TranslationPipe,
        {
            provide: APP_BASE_HREF,
            useValue: getBaseHref()
        },
        {
            provide: LOCALE_ID,
            deps: [TranslationService],
            useFactory: (ts: TranslationService) => ts.language
        }

    ],
    entryComponents: [
        ConfirmDialogComponent,
    ],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppModule {
}

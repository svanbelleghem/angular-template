export const environment = {
    production: false,
    APP_ID: "cm-angular-template",
    APP_ORIGIN: "https://www.cm.com",
    OLD_ORIGIN: "https://campaigns.cmtelecom.com",

    AUTH_URL: "https://login.cm.com",
    API_URL: "https://api.cm.com",
    TRANSLATION_SCOPE: "cm-template",
    TRANSLATION_URL: "https://api.cm.com",
};

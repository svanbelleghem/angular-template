export const environment = {
    production: false,
    APP_ID: "cm-angular-template",
    APP_ORIGIN: "https://dev.cmtest.nl:1443",
    OLD_ORIGIN: "https://cm-angular-template.dev.cmtest.nl:1443",

    AUTH_URL: "https://login.cmtest.nl",
    API_URL: "https://api.cmtest.nl",
    TRANSLATION_SCOPE: "cm-template",
    TRANSLATION_URL: "https://api.cm.com",
};

interface Window {
    AddSuccessNotification(message: string): void;
    AddErrorNotification(message: string): void;
}

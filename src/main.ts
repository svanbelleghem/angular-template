import { enableProdMode } from "@angular/core";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { AppModule } from "./app/app.module";
import { environment } from "./environments/environment";
import { AccountService, TranslationService, IdentityService } from "./app/services";
import * as moment from "moment";
import { HelloWorldService, GlobalizationService } from "./app/clients";

async function main() {
    try {
        // Redirect to apps-proxy if bypassed
        if (window.location.origin === environment.OLD_ORIGIN) {
            const parts = window.location.pathname.split("/");

            let languagePart = "";
            if (parts[1] === "en") {
                languagePart = "/en-gb";
            } else if (parts[1] === "ja") {
                languagePart = "/ja-jp";
            } else if (/^[A-z]{2}$/.test(parts[1])) {
                languagePart = `/${parts[1]}-${parts[1]}`.toLowerCase();
            } else if (/^[A-z]{2}-[A-z]{1,2}$/.test(parts[1])) {
                languagePart = `/${parts[1]}`.toLowerCase();
            }

            const path = parts.splice(2).join("/");

            const href = `${environment.APP_ORIGIN}${languagePart}/app/${environment.APP_ID}/${path}${window.location.search}`;
            window.location.href = href;
            return;
        }

        await Promise.all([
            TranslationService.preload(environment.TRANSLATION_URL, environment.TRANSLATION_SCOPE),
            AccountService.preload(environment.API_URL, true),
            IdentityService.preload(environment.AUTH_URL),
        ]);

        moment.locale([TranslationService.Language, TranslationService.DefaultLanguage]);

        HelloWorldService.Url
            = GlobalizationService.Url
            = environment.API_URL;

        if (environment.production) {
            enableProdMode();
            TranslationService.EnableProdMode();
        }

        platformBrowserDynamic().bootstrapModule(AppModule);
    } catch (err) {
        console.log(err);
    }
}

main();
